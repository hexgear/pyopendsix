## Endpoints 

- [api/v1/info](#)
- [api/v1/attributes](#)
- [api/v1/base/attributes](#)
- [api/v1/properties](#)
- [api/v1/quirks](#)
