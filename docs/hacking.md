# Hacking

## Coding Style

This project adheres to [PEP 8 -- Style Guide for Python Code][pep-8].

### Pylint GitHook

A `pre-commit` [git hook][githook-ref] utilizing [pylint][pylint-ref], can be configured to report any errors.

```terminal
cp .githooks/pre-commit.py .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

## OpenD6 References

Here are just a few sites that contain reference material for the OpenD6 system:

- [Open D6 System Reference Document][opendsix-ref-doc]
- [OpenD6 Wikia][opendsix-wikia]
- [RPG Library][rpg-lib]

[pep-8]: https://www.python.org/dev/peps/pep-0008
[githook-ref]: https://git-scm.com/docs/githooks
[pylint-ref]: https://www.pylint.org
[opendsix-ref-doc]: http://opend6.wikidot.com
[opendsix-wikia]: https://opend6.fandom.com/wiki/Main_Page
[rpg-lib]: https://www.rpglibrary.org
