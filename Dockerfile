FROM python:alpine

RUN apk update && apk upgrade && \
    apk add gcc \
            libffi-dev \
            make \
            musl-dev \
            openssl-dev

WORKDIR /opt/pyopendsix
COPY . /opt/pyopendsix

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "-b0.0.0.0:8000", "wsgi:api"]
