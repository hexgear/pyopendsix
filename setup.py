from setuptools import setup, find_packages

VERSION = '0.0.4'

setup(name='pyopendsix',
      version=VERSION,
      description='RESTful Python API based on OpenD6',
      long_description=open("README.md").read(),
      url='https://gitlab.com/hexgear/pyopendsix',
      license='AGPL-3.0-or-later',
      author='Lucas Ramage',
      author_email='ramage.lucas@protonmail.com',
      packages=find_packages(),
      install_requires=['flask'],
      include_package_data=True,
      entry_points={
          'console_scripts': [
              'pyopendsix = pyopendsix.__main__:create_api'
          ]
      },
      zip_safe=False
)
