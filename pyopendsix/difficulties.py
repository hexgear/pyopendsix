#!/usr/env/python
# coding: utf-8

"""
Standard Difficulties
"""

def get_difficulty(action):
    """
    >>> difficulty = get_difficulty(action)
    """
    if action > 0 <= 5:
        difficulty = "very_easy"
    elif action >= 6 <= 10:
        difficulty = "easy"
    elif action >= 11 <= 15:
        difficulty = "moderate"
    elif action >= 16 <= 20:
        difficulty = "difficult"
    elif action >= 21 <= 25:
        difficulty = "very_difficult"
    elif action >= 26 <= 30:
        difficulty = "heroic"
    elif action >= 31:
        difficulty = "legendary"
    else:
        difficulty = "automatic"

    return difficulty
