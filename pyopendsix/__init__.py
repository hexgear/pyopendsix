#!/usr/env/python
# coding: utf-8

"""
OpenD6 Flask API
"""

import logging
from os import path
from json import load
from flask import Flask, redirect, url_for, jsonify

def get_data():
    """Retrieve OpenD6 Data from file
    >>> get_data()
    """
    with open(path.join(path.dirname(__file__), 'schema.json')) as opendsix_data:
        return load(opendsix_data)

def define_api():
    """Define API endpoints
    >>> define_api()
    """
    api = Flask(__name__)
    data = get_data()

    # Configure logging
    gunicorn_logger = logging.getLogger('gunicorn.error')
    api.logger.handlers = gunicorn_logger.handlers
    api.logger.setLevel(gunicorn_logger.level)

    api.url_map.strict_slashes = False

    @api.route('/')
    def _redirect_info():
        """Redirect to OpenD6 information route.
        >>> _redirect_info()
        """
        return redirect(url_for('_get_api_info'))

    @api.route('/info')
    def _get_api_info():
        """Return api information for OpenD6.
        >>> _get_api_info()
        """
        api_info = {"opendsix" : data}
        return jsonify(api_info)

    @api.route('/attributes')
    def _get_attributes():
        """Return all attributes for OpenD6.
        >>> _get_attributes()
        """
        return jsonify(data['attributes'])

    @api.route('/attributes/<string:attribute_name>')
    def _get_attribute_description(attribute_name):
        """Return attribute description based on name.
        >>> _get_attribute_description(attribute_name)
        """
        return jsonify(data['attributes'][attribute_name])

    @api.route('/properties')
    def _get_properties():
        """Return all properties for OpenD6.
        >>> _get_properties()
        """
        return jsonify(data['properties'])

    @api.route('/properties/<string:property_name>')
    def _get_property_value(property_name):
        """Return default property value.
        >>> _get_property_value(property_name)
        """
        return jsonify(data['properties'][property_name])

    @api.route('/quirks')
    def _get_quirks():
        """Return all quirks for OpenD6.
        >>> quirks = _get_quirks()
        type(quirks) == string
        """
        return jsonify(data['quirks'])

    @api.route('/quirks/<int:quirk_id>')
    def _get_quirk(quirk_id):
        """Return quirk based on id.
        >>> _get_quirk(0)
        """
        return jsonify(data['quirks'][quirk_id])

    @api.route('/difficulties')
    def _get_difficulties():
        """Return all difficulties for OpenD6.
        >>> difficulties = _get_difficulties()
        type(difficulties) == string
        """
        return jsonify(data['difficulties'])

    @api.route('/difficulties/<string:difficulty_name>')
    def _get_difficulty(difficulty_name):
        """Return difficulty based on name.
        >>> _get_quirk("automatic")
        """
        return jsonify(data['difficulties'][difficulty_name])

    @api.route('/variants')
    def _get_variants():
        """Return all variants for OpenD6.
        >>> variants = _get_variants()
        type(variants) == string
        """
        return jsonify(data['variants'])

    @api.route('/variants/<string:variant_name>')
    def _get_variant(variant_name):
        """Return variant based on name.
        >>> _get_variant("classic")
        """
        return jsonify(data['variants'][variant_name])

    return api
