#!/usr/env/python
# coding: utf-8

"""
Test Wound Levels
"""

import unittest
from pyopendsix import wounds

class TestWounds(unittest.TestCase):

    def test_non_numeric_effect(self):
        self.fail("Not yet implemented.")

    def test_negative_effect(self):
        self.fail("Not yet implemented.")

    def test_bruised_effect(self):
        bruised = wounds.get_effect(0)
        self.assertEqual(bruised, 'bruised')

    def test_stunned_effect(self):
        for damage in range(1,3):
            stunned = wounds.get_effect(damage)
            self.assertEqual(stunned, 'stunned')

    def test_wounded_effect(self):
        for damage in range(4,6):
            wounded = wounds.get_effect(damage)
            self.assertEqual(wounded, 'wounded')

    def test_severely_wounded_effect(self):
        for damage in range(7,8):
            severely_wounded = wounds.get_effect(damage)
            self.assertEqual(severely_wounded, 'severely_wounded')

    def test_incapacitated_effect(self):
        for damage in range(9,12):
            incapacitated = wounds.get_effect(damage)
            self.assertEqual(incapacitated, 'incapacitated')

    def test_mortally_wounded_effect(self):
        for damage in range(13,15):
            mortally_wounded = wounds.get_effect(damage)
            self.assertEqual(mortally_wounded, 'mortally_wounded')

    def test_dead_effect(self):
        for damage in range(16,20):
            dead = wounds.get_effect(damage)
            self.assertEqual(dead, 'dead')

if __name__ == '__main__':
    unittest.main()
